# AngSpa #

How to get up and running with this codebase

### What is this repository for? ###

Sandboxing a setup with ANG inside an MVC app.

### How do I get set up? ###

* Install NodeJS
* Run 'npm install' top install node packages
* Install Bower 'npm install -g bower'
* Install Bower packages 'bower install'
* Build VS Sln (which will pull nuget packages)
* Setup in IIS

### Trouble with Bower? ###

I dont know for sure but I think this may be VS2015 causing the issue. Run:

* npm cache clean
* bower cache clean
* bower install