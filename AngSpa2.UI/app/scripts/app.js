'use strict';

/**
 * @ngdoc overview
 * @name yapp
 * @description
 * # yapp
 *
 * Main module of the application.
 */
angular
  .module('yapp', [
    'ui.router',
    'ngAnimate'
  ])
  .config(function($stateProvider, $urlRouterProvider) {

    // Default dashboard route
    $urlRouterProvider.when('/dashboard', '/dashboard/overview');
    $urlRouterProvider.otherwise('/dashboard/overview');

    $stateProvider
      .state('base', {
        abstract: true,
        url: '',
        templateUrl: '/app/views/base.html'
      })
        .state('dashboard', {
          url: '/dashboard',
          parent: 'base',
          templateUrl: '/app/views/dashboard.html',
          controller: 'DashboardCtrl'
        })
          .state('overview', {
            url: '/overview',
            parent: 'dashboard',
            templateUrl: '/app/views/dashboard/overview.html'
          })
          .state('reports', {
            url: '/reports',
            parent: 'dashboard',
            templateUrl: '/app/views/dashboard/reports.html'
          });

  });
