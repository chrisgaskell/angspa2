﻿using System.Web.Mvc;

namespace AngSpa2.UI.Controllers
{
    public class RootController : Controller
    {
        [Route("")]
        [HttpGet]
        public ViewResult Index()
        {
            return View();
        }
    }
}