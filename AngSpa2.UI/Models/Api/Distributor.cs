﻿namespace AngSpa2.UI.Models.Api
{
    public class Distributor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}